package fi.vamk.e1801699;


import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    ReferenceValidator refVal = new ReferenceValidator();
    
    
    @Test
    public void refereceShouldBeOverThreeNumbers(){
        String ref = refVal.getReferenceNumber("123");
        assertTrue(ref.length() > 3);
        
        ref = refVal.getReferenceNumber("12");
        assertEquals("Invoice number has to be 3-18 digits", ref);
    }
    
    @Test
    public void refereceShouldBeUnderTwentyNumbers(){
        String ref = refVal.getReferenceNumber("123456789012345678");
        String[] pieces = ref.split(" ");
        ref = "";
        for (String piece : pieces) {
            ref += piece;
        }
        assertTrue(ref.length() < 20);
        
        ref = refVal.getReferenceNumber("1234567890123456789");
        
        assertEquals("Invoice number has to be 3-18 digits", ref);
    }
    
    @Test
    public void refereceShouldBeGrouped(){
        String ref = refVal.getReferenceNumber("34623636236423");
        
        boolean grouped = true;
        int j = 1;
        for(int i = (ref.length()-1); i >= 0; i--) {
            if(j%6==0) {
                String ch = "" + ref.charAt(i);
                
                if(!ch.equals(" ")) {
                    grouped = false;
                }    
            }
            j++;    
        }
        
        assertTrue(grouped);
        
        //same code with wrongly grouped reference number
        ref = "342 3463 34";
        grouped = true;
        j = 1;
        for(int i = (ref.length()-1); i >= 0; i--) {
            if(j%6==0) {
                String ch = "" + ref.charAt(i);
                
                if(!ch.equals(" ")) {
                    grouped = false;
                }    
            }
            j++;    
        }
        
        assertFalse(grouped);    
    }
    
    @Test
    public void refereceShouldContainOnlyNumbersAndSpaces(){
        String ref = refVal.getReferenceNumber("143587257131346");
        String[] pieces = ref.split(" ");
        String refWtihoutSpaces = "";
        for(String piece: pieces) {
            refWtihoutSpaces += piece;
        }
        
        assertTrue(refWtihoutSpaces.matches("([0-9])*"));
        
        ref = refVal.getReferenceNumber("dfq3463g");
        
        assertEquals("Invoice number should only contain numbers", ref);
        
    }
    
    @Test
    public void checkingNumberShouldBeRight(){
        //checking numbers calculated with: 
        //https://pankki.nordea.fi/yritysasiakkaat/maksut/laskutus-ja-maksaminen/viitelaskin.flex
        
        String ref = refVal.getReferenceNumber("374258568");
        String checkNum = "" + ref.charAt(ref.length()-1);
        
        assertEquals("6", checkNum);
        
        ref = refVal.getReferenceNumber("123");
        checkNum = "" + ref.charAt(ref.length()-1);
        
        assertEquals("2", checkNum);
        
        ref = refVal.getReferenceNumber("23472468248258245");
        checkNum = "" + ref.charAt(ref.length()-1);
        
        assertEquals("4", checkNum);
           
       
    }
    
    
    
    
    
}

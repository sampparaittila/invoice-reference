package fi.vamk.e1801699;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Scanner reader = new Scanner(System.in);
        ReferenceValidator validator = new ReferenceValidator();
        
        while(true) {
            System.out.println("");
            System.out.println("");
            System.out.println("Give your invoice number without spaces (-1 to exit):");
            System.out.print(">> ");
            String invNum = reader.nextLine();
            
            if(invNum.equals("-1")) {
                break;
            }
        
            System.out.println("Reference number:");
            System.out.println(validator.getReferenceNumber(invNum));
        }
        
        System.out.println("Good bye!");
           
    }
}

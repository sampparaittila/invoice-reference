/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.vamk.e1801699;

/**
 *
 * @author sampp
 */
public class ReferenceValidator {
    
    public String getReferenceNumber(String invoiceNumber) {
        
        if (invoiceNumber.length() > 18 || invoiceNumber.length() < 3) {
            return "Invoice number has to be 3-18 digits";
        }
        
        if(!invoiceNumber.matches("([0-9])*")) {
            return "Invoice number should only contain numbers";
        }
        
        int checkNum = getCheckingNumber(invoiceNumber);
        
        StringBuilder refNum = new StringBuilder();
        
        refNum.append(checkNum);
        int counter = 1;
        
        for(int i=(invoiceNumber.length()-1);i>=0;i--) {
            if(counter%5 == 0) {
                refNum.append(" ");
            }
            refNum.append(invoiceNumber.charAt(i));
            counter++;
        }
        
        
        return refNum.reverse().toString();
    }
    
    private int getCheckingNumber(String invoiceNumber) {
        int[] invoiceArray = getStringAsIntArray(invoiceNumber);
        int[] multipliers = getMultipliers(invoiceNumber.length());
        
        int refNum = 0;
        
        for (int i = 0; i<invoiceArray.length; i++) {
            refNum += invoiceArray[i]*multipliers[i];
        }
        
        int j = refNum;
        while(j%10 != 0) {
            j++;
        }
        
        return j-refNum;
    }
    
    private int[] getStringAsIntArray(String str) {
        int[] intArray = new int[str.length()];
        String c;
        int j = 0;
        for(int i=(str.length()-1); i>=0; i--) {
            c = ""+str.charAt(i);
            intArray[j] = Integer.parseInt(c);
            j++;
        }
        return intArray;
    }
    
    private int[] getMultipliers(int lenght) {
        int[] multipliers = new int[lenght];
        
        for (int i=0; i<lenght; i++) {
            if ((i+3)%3==0) {
                multipliers[i] = 7;  
            } else if ((i+2)%3==0) {
                multipliers[i] = 3;
            } else {
                multipliers[i] = 1;
            }
        }
        
        return multipliers;
    }
}
